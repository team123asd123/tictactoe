var assert = require('assert');
var ObjectID = require('mongodb').ObjectID;
var connect = require('./connect');
var config = require('./config.json');
const CROSS = 1, CIRCLE = 2, EMPTY = 0;


var changePassword = function (session, login, oldpassword, newpassword) {
    try {
        getSessionOwnerIfValid(session, function (dane) {
            var o_id = new ObjectID(dane);
            connect.mongo_update('Konta', {
                "login": login,
                "_id": o_id,
                "password": oldpassword
            }, {
                    $set: {
                        "password": newpassword
                    }
                }, function (data) {
                    if (data) {
                        sendEmailToAccount(dane, config.email, "Password was changed", "Pasword to your acount was change from:" + oldpassword + " ,to: " + newpassword + " .", "");
                        extendSessionTime(session);
                    } else {

                    }
                });
        });
    }
    catch (e) {
        console.log(e);
    }
};
var removeAccount = function (session, login, password) {
    try {
        getSessionOwnerIfValid(session, function (dane) {
            var o_id = new ObjectID(dane);
            connect.mongo_delete('Konta', {
                "_id": o_id,
                "login": login,
                "password": password
            }, function (data) {

            })
        });
    }
    catch (e) {
        console.log(e);
    }
};
var createSession = function (owner, callback) {
    try {
        var id = new ObjectID();
        var date = new ObjectID().getTimestamp();
        connect.mongo_insert('Sessions', {
            "_id": id,
            "owner": owner,
            "valid_date": date
        }, function (data) {
            if (data)
                callback(id);
        })
    }
    catch (e) {
        console.log(e);
    }
};
var logowanie = function (login, password, callback) {
    try {
        connect.mongo_find('Konta', {
            "login": login,
            "password": password
        }, function (data) {
            if (data != null && data != false) {
                if (data[0].status != "new") {
                    createSession(data[0]._id, function (dane) {
                        callback(dane);
                    });
                } else {
                    callback('Verifacation required');
                }
            } else {
                callback("failed");
            }
        })
    }
    catch (e) {
        console.log(e);
        callback("error");
    }
};
var verify = function (login, mail, callback) {
    try {
        connect.mongo_update('Konta', {
            "login": login,
            "mail": mail,
            "status": "new"
        }, {
                $set: {
                    "status": "verified"
                }
            }, function (data) {
                if (data) {
                    connect.mongo_find('Konta', {
                        "login": login
                    }, function (data2) {
                        if (data2 != null && data2 != false) {
                            sendEmailToAccount(data2[0]._id, config.email, "Account Activaction", "Your account was succesfull activated.", "");
                            callback(true);
                        }
                    })
                } else {
                    callback(false);
                }

            })
    }
    catch (e) {
        console.log(e);
        callback(false);
    }
};
var registerAccount = function (login, password, mail, callback) {
    try {
        connect.mongo_find('Konta', {
            $or: [{ "login": login }, { "email": mail }]
        }, function (data) {
            if (data == null || data == false) {
                connect.mongo_insert('Konta', {
                    "login": login,
                    "password": password,
                    "mail": mail,
                    "status": "new"
                }, function (data2) {
                    if (data2) {
                        callback("success")
                        sendEmail(mail, config.email, "Verify your TICTACTOE account", "", "<a href='" + config.server_address + "/verify?login=" + login + "&mail=" + mail + "'>Verify your account</a>");
                    } else {
                        callback("failed")
                    }
                })
            } else {
                callback("failed")
            }
        })
    }
    catch (e) {
        callback("failed")
        console.log(e);
    }
};
var getSessionOwnerIfValid = function (session_id, callback) {
    try {
        var id = new ObjectID(session_id);
        connect.mongo_find('Sessions', {
            "_id": id
        }, function (data) {
            if (data != null && data != false) {
                try {
                    var date_session = new Date(ObjectID(id).getTimestamp());
                    var data_now = new Date(ObjectID().getTimestamp());
                    var diff = data_now - date_session;
                    //console.log(Math.floor(diff / 60e3))
                    if (diff > 60e3) {
                        if (Math.floor(diff / 60e3) <= 10080) {
                            callback(data[0].owner);
                        }
                    }
                    else {
                        callback(data[0].owner);
                    }
                }
                catch (e) {
                    console.log(e);
                    callback(undefined);
                }
            } else {
                callback(undefined);
            }
        })
    }
    catch (e) {
        console.log(e);
    }
};
var sendEmailToAccount = function (user, odkogo, temat, wiadomosc, html) {
    try {
        'use strict';
        var dokogo;
        const nodemailer = require('nodemailer');
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: config.email_login,
                pass: config.email_password
            }
        });
        var o_id = new ObjectID(user);
        connect.mongo_find('Konta', {
            "_id": o_id
        }, function (data) {
            if (data != null && data != false) {
                try {
                    dokogo = data[0].mail.toString();
                    let mailOptions = {
                        from: odkogo, // sender address
                        to: dokogo, // list of receivers
                        subject: temat, // Subject line
                        text: wiadomosc, // plain text body
                        html: html // html body
                    };
                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            return console.log(error);
                        }
                        console.log('Message %s sent: %s', info.messageId, info.response);
                    });
                }
                catch (e) {
                    console.log(e);
                }
            }
        })
    }
    catch (e) {
        console.log(e);
    }
};
var sendEmail = function (dokogo, odkogo, temat, wiadomosc, html) {
    try {
        'use strict';
        const nodemailer = require('nodemailer');
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: config.email_login,
                pass: config.email_password
            }
        });
        let mailOptions = {
            from: odkogo, // sender address
            to: dokogo, // list of receivers
            subject: temat, // Subject line
            text: wiadomosc, // plain text body
            html: html // html body
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
        });
    }
    catch (e) {
        console.log(e);
    }
};
var extendSessionTime = function (session_id) {
    try {
        var data_now = new Date(ObjectID().getTimestamp());
        var id = new ObjectID(session_id);
        connect.mongo_update('Sessions', {
            "_id": id,
        }, {
                $set: {
                    "valid_date": data_now
                }
            }, function () { })
    }
    catch (e) {
        console.log(e);
    }
};
function getTable2D(x, y, callback) {
    var tab = [];
    for (var i = 0; i < x; i++) {
        tab[i] = new Array (y);
    }
    for (var i = 0; i < x; i++) {
        for (var c = 0; c < y; c++) {
            tab[i][c] = 0
        }
    }
    callback(tab);
}
var createGame = function (session_id, room_name, width, height, howmanytowin, callback) {
    try {
        extendSessionTime(session_id)
        get_board_and_whos_move_and_players_howmanytowin(room_name, function (data) {
            if (data == "error") {
                getSessionOwnerIfValid(session_id, function (data) {
                    getTable2D(width, height, function (data2) {
                        connect.mongo_insert('Games', {
                            "player1": data,
                            "player2": null,
                            "room_name": room_name,
                            "board": data2,
                            "whos_move": CROSS,
                            "howmanytowin": howmanytowin
                        }, function (data3) {
                            if (data3) {
                                callback("success")
                            } else {
                                callback("failed")
                            }
                        })
                    });
                })
            } else {
                callback("failed")
            }
        })
    }
    catch (e) {
        console.log(e);
        callback("failed")
    }
}
var connectToTheGame = function (session_id, room_name, callback) {
    try {
        extendSessionTime(session_id)
        getSessionOwnerIfValid(session_id, function (data) {
            try {
                connect.mongo_update('Games', {
                    "room_name": room_name,
                    "player2": null
                }, {
                        $set: {
                            "player2": data
                        }
                    }, function (data2) {
                        if (data2) {
                            get_board_and_whos_move_and_players_howmanytowin(room_name, function (data) {
                                if (data != null && data != undefined && data != "error") {
                                    callback("success")
                                } else {
                                    callback("error")
                                }
                            })
                        } else {
                            callback("error")
                        }

                    })
            } catch (e) {
                console.log(e)
                callback("failed")
            }
        });

    }
    catch (e) {
        console.log(e);
        callback("failed")
    }
}
var get_board_and_whos_move_and_players_howmanytowin = function (room_name, callback) {
    try {
        connect.mongo_find('Games', {
            "room_name": room_name
        }, function (data) {
            if (data != null && data != false) {
                callback(data[0].board, data[0].whos_move, data[0].player1, data[0].player2, data[0].howmanytowin)
            }
            else {
                callback("error")
            }
        })
    }
    catch (e) {
        console.log(e);
        callback("error")
    }
}
var move = function (session_id, room_name, whereX, whereY, callback) {
    try {
        extendSessionTime(session_id)
        getSessionOwnerIfValid(session_id, function (data) {
            get_board_and_whos_move_and_players_howmanytowin(room_name, function (board, whos_move, p1, p2, howmanytowin) {
                if (whos_move == CROSS && ("" + p1).trim() != ("" + data).trim() && ("" + p2).trim() == ("" + data).trim()) {
                    callback("error")
                } else if (whos_move == CIRCLE && ("" + p1).trim() == ("" + data).trim() && ("" + p2).trim() != ("" + data).trim()) {
                    callback("error")
                } else if ((whos_move == CIRCLE && ("" + p1).trim() != ("" + data).trim() && ("" + p2).trim() == ("" + data).trim()) || (whos_move == CROSS && ("" + p1).trim() == ("" + data).trim() && ("" + p2).trim() != ("" + data).trim())) {
                    var id = new ObjectID(data);
                    if (board[whereX][whereY] == EMPTY) {
                        board[whereX][whereY] = whos_move
                        if (whos_move == CROSS) {
                            whos_move = CIRCLE
                            connect.mongo_update('Games', {
                                "room_name": room_name,
                                "player1": id
                            }, {
                                    $set: {
                                        "board": board,
                                        "whos_move": whos_move
                                    }
                                },
                                function (data) {
                                    if (data) {
                                        checkBoardForWin(board, howmanytowin, function (data) {
                                            callback(board, data)
                                        })
                                    }
                                })
                        } else {
                            whos_move = CROSS
                            connect.mongo_update('Games', {
                                "room_name": room_name,
                                "player2": id

                            }, {
                                    $set: {
                                        "board": board,
                                        "whos_move": whos_move
                                    }
                                },
                                function (data) {
                                    if (data) {
                                        checkBoardForWin(board, howmanytowin, function (data) {
                                            callback(board, data)
                                        })
                                    }
                                })
                        }
                    } else {
                        console.log("error");
                        callback("error")
                    }
                }
            })
        });
    }
    catch (e) {
        console.log(e);
    }
}
var deleteGame = function (room_name) {
    try {
        connect.mongo_delete('Games', {
            "room_name": room_name
        }, function () { })
    }
    catch (e) {
        console.log(e);
    }
}
function checkRightDiagonal(sign, howManyToWin, board) {
    for (var y = 0; y < board[0].length - howManyToWin + 1; y++) {
        for (var x = 0; x < board.length - howManyToWin + 1; x++) {
            var correctCount = 0;
            for (var i = 0; i < howManyToWin; i++)
                if (board[x + i][y + i] == sign)
                    correctCount++;
            if (correctCount == howManyToWin)
                return true;
        }
    }
    return false;
}
function checkHorizontal(sign, howManyToWin, board) {
    for (var y = 0; y < board[0].length; y++) {
        for (var x = 0; x < board.length - howManyToWin + 1; x++) {
            var correctCount = 0;
            for (var i = 0; i < howManyToWin; i++)
                if (board[x + i][y] == sign)
                    correctCount++;
            if (correctCount == howManyToWin)
                return true;
        }
    }
    return false;
}
function checkVertical(sign, howManyToWin, board) {
    for (var y = 0; y < board[0].length - howManyToWin + 1; y++) {
        for (var x = 0; x < board.length; x++) {
            var correctCount = 0;
            for (var i = 0; i < howManyToWin; i++)
                if (board[x][y + i] == sign)
                    correctCount++;
            if (correctCount == howManyToWin)
                return true;
        }
    }
    return false;
}
function checkLeftDiagonal(sign, howManyToWin, board) {
    for (var y = 0; y < board[0].length - howManyToWin + 1; y++) {
        for (var x = howManyToWin - 1; x < board.length; x++) {
            var correctCount = 0;
            for (var i = 0; i < howManyToWin; i++)
                if (board[x - i][y + i] == sign)
                    correctCount++;
            if (correctCount == howManyToWin)
                return true;
        }
    }
    return false;
}

var checkBoardForWin = function (board, howmanytowin, callback) {
    var draw = 0;
    var winner = "no one";
    for (var x = 0; x < board.length; x++) {
        for (var y = 0; y < board[0].length; y++) {
            if (board[x][y] == CROSS || board[x][y] == CIRCLE) {
                draw++
            }
        }
    }
    if (checkHorizontal(CIRCLE, howmanytowin, board) || checkVertical(CIRCLE, howmanytowin, board)) {
        winner = CIRCLE
    } else
        if (checkHorizontal(CROSS, howmanytowin, board) || checkVertical(CROSS, howmanytowin, board)) {
            winner = CROSS
        } else
            if (checkRightDiagonal(CROSS, howmanytowin, board) || checkLeftDiagonal(CROSS, howmanytowin, board)) {
                winner = CROSS
            } else
                if (checkRightDiagonal(CIRCLE, howmanytowin, board) || checkLeftDiagonal(CIRCLE, howmanytowin, board)) {
                    winner = CIRCLE
                }
    if (draw == board.length * board[0].length) {
        winner = "draw"
    }
    callback(winner)
}
var getSessionValid = function (session_id, callback) {
    getSessionOwnerIfValid(session_id, function (data) {
        if (data != undefined) {
            callback(true);
        }
        else {
            callback(false);
        }
    });
};
var getAllRoomList = function (callback) {
    try {
        connect.mongo_find('Games', {}, function (data) {
            if (data != null && data != false) {
                callback(data)
            } else {
                callback([])
            }
        })
    }
    catch (e) {
        console.log(e);
        callback("error")
    }
}
module.exports.get_board_and_whos_move_and_players_howmanytowin = get_board_and_whos_move_and_players_howmanytowin;
module.exports.getAllRoomList = getAllRoomList;
module.exports.checkBoardForWin = checkBoardForWin;
module.exports.createGame = createGame;
module.exports.move = move;
module.exports.connectToTheGame = connectToTheGame;
module.exports.deleteGame = deleteGame;
module.exports.getSessionValid = getSessionValid;
module.exports.sendEmailToAccount = sendEmailToAccount;
module.exports.sendEmail = sendEmail;
module.exports.changePassword = changePassword;
module.exports.removeAccount = removeAccount;
module.exports.logowanie = logowanie;
module.exports.registerAccount = registerAccount;
module.exports.verify = verify;