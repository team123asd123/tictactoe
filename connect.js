var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var mongo_insert = function (collection, requirements, callback) {
    connect(function (data,db) {
        if (data) {
            try {
                db.collection(collection).insertOne(requirements);
                callback(true)
            } catch (error) {
                console.log(error)
                callback(false)
            }
            try {
                db.close();
            }
            catch (e) {
                console.log(e);
            }
        } else {
            callback(false)
        }
    });
};
var mongo_delete = function (collection, requirements, callback) {
    connect(function (data,db) {
        if (data) {
            try {
                db.collection(collection, {}, function (err, contacts) {
                    if (!err) {
                        contacts.remove(requirements, function (err, result) {
                            if (err) {
                                callback(false)
                            } else {
                                callback(true)
                            }
                        });
                    }
                });
            } catch (error) {
                console.log(error)
                callback(false)
            }
            try {
                db.close();
            }
            catch (e) {
                console.log(e);
            }
        } else {
            callback(false)
        }
    });
};
var mongo_update = function (collection, requirements, set, callback) {
    connect(function (data,db) {
        if (data) {
            try {
                db.collection(collection).updateOne(requirements, set,
                    function (err, result) {
                        if (!err)
                            callback(true)
                        else
                            callback(false)
                    });
            } catch (error) {
                console.log(error)
                callback(false)
            }

            try {
                db.close();
            }
            catch (e) {
                console.log(e);
            }
        } else {
            callback(false)
        }
    });
};
var mongo_find = function (collection, requirements, callback) {
    connect(function (data,db) {
        if (data) {
            try {
                var cursor = db.collection(collection).find(requirements).toArray(function (err, doc) {
                    assert.equal(err, null);
                    if (doc[0] != null) {
                        callback(doc)
                    } else {
                        callback(false)
                    }
                });
            } catch (error) {
                console.log(error)
                callback(false)
            }

            try {
                db.close();
            }
            catch (e) {
                console.log(e);
            }
        } else {
            callback(false)
        }
    });
};
function connect(callback) {
    try {
        MongoClient.connect("mongodb://localhost:27017/XO", function (err, db) {
            callback(true,db,err)
        });
    } catch (e) {
        callback(false)
        console.log(e);
    }
}
module.exports.mongo_insert = mongo_insert;
module.exports.mongo_delete = mongo_delete;
module.exports.mongo_update = mongo_update;
module.exports.mongo_find = mongo_find;