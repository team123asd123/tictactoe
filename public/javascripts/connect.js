
var socket = io.connect('http://werty12121.redirectme.net:665/');

var createTableRoomInput = document.getElementById("create_table_room_input");
var widthInput = document.getElementById("width_input");
var heightInput = document.getElementById("height_input");
var howManyToWinInput = document.getElementById("how_many_to_win_input");
var createTableButton = document.getElementById("create_table_button");

var joinTableRoomInput = document.getElementById("join_table_room_input");
var joinTableButton = document.getElementById("join_table_button");

var roomList = document.getElementById("room_list");
var roomListElements = document.getElementsByClassName("room_list_element");
var refreshButton = document.getElementById("refresh_button");

createTableButton.onclick = function(){
	
	var roomName = createTableRoomInput.value;
	if(roomName.length < 5){
		alert("Room name is too short");
		return;
	}
	if(roomName.length > 25){
		alert("Room name is too long");
		return;
	}
	var width = Number(widthInput.value);
	if(isNaN(width)){
		alert("Width is not a number");
		return;
	}
	if(width < 3){
		alert("Width is to small");
		return;
	}
	if(width > 10){
		alert("Width is to big");
		return;
	}
	var height = Number(heightInput.value);
	if(isNaN(height)){
		alert("Height is not a number");
		return;
	}
	if(height < 3){
		alert("Height is to small");
		return;
	}
	if(height > 10){
		alert("Height is to big");
		return;
	}

	var howManyToWin = Number(howManyToWinInput.value);
	if(isNaN(howManyToWin)){
		alert("How many to win is not a number");
		return;
	}
	if(howManyToWin < 3){
		alert("How many to win is to small");
		return;
	}
	if(howManyToWin > 10){
		alert("How many to win is to big");
		return;
	}
	if(howManyToWin > Math.max(width, height)){
		alert("How many to win exceeds width and height");
		return;
	}

	var session = getCookie("session");
	
	//console.log("Create Room: " + createTableRoomInput.value);
	//console.log("session: " + session);

	socket.emit("create_game", {
		"session": session,
		"room_name": roomName,
		"width": width,
		"height": height,
		"how_many_to_win": howManyToWin
	});

	setCookie("room_name", roomName, 1);
	setCookie("player", 1, 1);
}

socket.on("create_game_response", function(data){
	console.log("create_game_response");
	console.log(data);
	if(data.status == "success"){
		console.log("success");	
		window.location.href = "./game";
	}else{
		alert("failed");
	}
});

joinTableButton.onclick = function(){
	
	var roomName = joinTableRoomInput.value.trim();
	if(roomName.length < 5){
		alert("Room name is too short");
		return;
	}
	if(roomName.length > 25){
		alert("Room name is too long");
		return;
	}
	
	joinRoom(roomName);
}

function joinRoom(roomName){
	var session = getCookie("session");
	
	console.log("Join Room: " + joinTableRoomInput.value);
	console.log("session: " + session);

	socket.emit("join_game", {"session": session, "room_name": roomName});

	setCookie("room_name", roomName, 1);
	setCookie("player", 2, 1);
}

socket.on("join_game_response", function(data){
	console.log("join_game_response");
	console.log(data);
	if(data.status == "success"){
		console.log("success")
		window.location.href = "./game";
	}else{
		alert("Error connecting");
	}
});

refreshButton.onclick = refreshRooms;

function refreshRooms(){
	//console.log("refresh");
	socket.emit("get_all_rooms", {});
}

socket.on("get_all_rooms_response", function(data){
	//console.log("rooms data: ");
	//console.log(data);
	roomList.innerHTML = "";
	for(var i = 0; i < data.rooms.length; i++){
		var room = data.rooms[i];
		var el = document.createElement("div");
		el.className = "room_list_element";
		el.onclick = function(){
			console.log(el.placeholder);
			joinRoom(el.placeholder);
		};
		var full = room.player2 && room.player2.length != 0;
		el.innerHTML = room.room_name + " [" + (full ? "full" : "1/2") + "]";
		el.placeholder = room.room_name;
		roomList.appendChild(el);
	}
});

setInterval(refreshRooms, 5000);

refreshRooms();